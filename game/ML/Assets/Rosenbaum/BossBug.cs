﻿using UnityEngine;
using System.Collections;
/*
 * Anthony Rosenbaum November 12-17 2014
 * vincismurf@gmail.com
 * 859 948 835
 * 
 * Derived from Base Bug:
 *  Only differance is the texture and strength 
 */ 
public class BossBug : BaseBug
{
    #region OVERRIDE_METHODS
    // Use this for initialization
	protected override void Start () {
        max = 1;
        strength = 5;
        base.Start();
	}
	
	// Update is called once per frame
    protected override void Update()
    {
        base.Update();
	}
    // Pass Along to Parent
    public override void clickBug()
    {
        base.clickBug();
    }
    // Pass Along to Parent
    public override void doubleClickBug()
    {
        base.doubleClickBug();
    }
    // Callback from parent  to Increment, and play sound
    protected override void kill()
    {
        gM.incBossBugCount();
        if (squash)
        {
            squash.Play();
        }
    }
    #endregion OVERRIDE_METHODS
}
