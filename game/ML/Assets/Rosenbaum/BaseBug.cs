﻿using UnityEngine;
using System.Collections;
/* * 
 * Anthony Rosenbaum November 12-17 2014
 * vincismurf@gmail.com
 * 859 948 835
 * 
 * Base for all bugs, has movement and clean up logic
 * Basic attributes are:
 *  strength = how many clicks until the bug is squashed
 *  speed = random value between min and max, used to scale forward vector for movement
 *  
 */
public class BaseBug : MonoBehaviour
{
    #region PROTECTED_MEMBER_VARIABLES
    protected float max = 0.6f;
    protected int strength = 1; 
    protected GameManager gM;
    protected AudioSource squash;
    #endregion PROTECTED_MEMBER_VARIABLES
    #region PRIVATE_MEMBER_VARIABLES
    private float min = 0.1f;
    private BugSpawner bS;
    private float speed;
    #endregion PRIVATE_MEMBER_VARIABLES
    #region VIRTUAL_METHODS
    //----------------------------------
    // Virtual Methods 
    //----------------------------------
    // Pass along to handler
    public virtual void clickBug()
    {
        handleClick();
    }
    // Pass along to handler
    public virtual void doubleClickBug()
    {
        handleClick();
    }
    // Callback to Increment, and play sound
    protected virtual void kill()
    {
        gM.incBaseBugCount();
        if (squash)
        {
            squash.Play();
        }
    }
    // Get GameManager, BugSpawner, and Camera. Determine a random speed
	protected virtual void Start () {
        gM = GameObject.Find("GameManager").GetComponent<GameManager>();
        bS = GameObject.Find("BugSpawner").GetComponent<BugSpawner>();
        //This is a hack, for some reason sounds would not play on objects unless the PlayAwake was set.
        squash = GameObject.Find("ARCamera").GetComponent<AudioSource>();

        speed = Random.Range(min, max);
	}
    // If you are on terrain move bug! otherwise Remove yourself.
    protected virtual void Update()
    {
        if (isOnTerrain() == true)
        {
            move();        
        }
        else 
        {
            removeMe(false);
        }
	}
    #endregion VIRTUAL_METHODS
    #region PROTECTED_METHODS
    // Decrease strength, if too low remove from the game
    protected void handleClick()
    {
        strength--;
        if (strength <= 0)
            removeMe(true);
    }
    #endregion PROTECTED_METHODS
    #region PRIVATE_METHODS
    //----------------------------------
    // Private Methods 
    //----------------------------------
    // Test to make sure Bug position is on the target.
    private bool isOnTerrain()
    {
        if (gM.getTerrain() == null)
            return false;
        bool b = false;
        Vector3 p = gameObject.transform.position;
        Vector3 t = gM.getTerrain().gameObject.transform.position;
        Vector2 size = gM.getTerrain().gameObject.transform.parent.GetComponent<ImageTargetBehaviour>().GetSize();
        if (p.x > (t.x - (size.x / 2)) && p.x < (t.x + (size.x / 2)) && p.z > (t.z - (size.y / 2)) && p.z < (t.z + (size.y / 2)))
            b = true;
        return b;
    }
    // Remove Bug, if incriment is true callback to update the scoreboard
    private void removeMe(bool inc)
    {
        if (bS.bugs.Contains(gameObject))
        {
            if (inc == true)
                kill();
            bS.bugs.Remove(gameObject);
            Destroy(gameObject, 0.0f);
        }
    }
    // Choose a Random Direction, and move forward
    private void move()
    {
        int rn = Random.Range(0, 100);
        if (rn % 3 == 0)
            transform.Rotate(new Vector3(0, 1, 0), 0.5f);
        if (rn % 4 == 0)
            transform.Rotate(new Vector3(0, 1, 0), -0.5f);

        transform.position -= transform.forward * speed;
    }
    #endregion PRIVATE_METHODS
}
