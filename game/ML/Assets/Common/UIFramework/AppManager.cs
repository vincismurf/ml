/*============================================================================== 
 * Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved. 
 * ==============================================================================*/
using UnityEngine;
using System.Collections;

/// <summary>
/// This class manages different views in the scene like AboutPage, SplashPage and ARCameraView.
/// All of its Init, Update and Draw calls take place via SceneManager's Monobehaviour calls to ensure proper sync across all updates
/// </summary>
public class AppManager : MonoBehaviour {
    
    #region PUBLIC_MEMBER_VARIABLES
    public string TitleForAboutPage = "Instructions";
    public string TitleForEndPage = "Statistics";
    public ISampleAppUIEventHandler m_UIEventHandler;
    public GameManager gM;
    #endregion PUBLIC_MEMBER_VARIABLES
    
    #region PROTECTED_MEMBER_VARIABLES
    public static ViewType mActiveViewType;
    public enum ViewType {SPLASHVIEW, ABOUTVIEW, UIVIEW, ARCAMERAVIEW, STATVIEW};
    #endregion PROTECTED_MEMBER_VARIABLES
    
    #region PRIVATE_MEMBER_VARIABLES
    private SplashScreenView mSplashView;
    private AboutScreenView mAboutView;
    private EndGameView mEndGameView;
    private float mSecondsVisible = 2.0f;
    #endregion PRIVATE_MEMBER_VARIABLES
    
    //This gets called from SceneManager's Start() 
    public virtual void InitManager () 
    {
        mSplashView = new SplashScreenView();
        mAboutView = new AboutScreenView();
        mEndGameView = new EndGameView();
        gM = GameObject.Find("GameManager").GetComponent<GameManager>();
        mAboutView.SetTitle(TitleForAboutPage);
        mEndGameView.SetTitle(TitleForEndPage);
        mAboutView.OnStartButtonTapped      += OnAboutStartButtonTapped;
        mEndGameView.OnRestartButtonTapped  += OnRestartButtonTapped;
        m_UIEventHandler.CloseView          += OnTappedOnCloseButton;
        m_UIEventHandler.GoToAboutPage      += OnTappedOnGoToAboutPage;
        gM.OnGameOver += OnEndGame;

        InputController.SingleTapped        += gM.singleTap;
        InputController.DoubleTapped        += gM.doubleTap;

        mSplashView.LoadView();
        StartCoroutine(LoadAboutPageForFirstTime());
        mActiveViewType = ViewType.SPLASHVIEW;
    }

    
    public virtual void UpdateManager()
    {
        if (ViewType.ARCAMERAVIEW == mActiveViewType)
        {
            if (!gM.hasStarted())
            {
                gM.startGame();
            }
        }
    }
    
    public virtual void Draw()
    {
        m_UIEventHandler.UpdateView(false);
        switch(mActiveViewType)
        {
            case ViewType.SPLASHVIEW:
                mSplashView.UpdateUI(true);
                break;
            
            case ViewType.ABOUTVIEW:
                mAboutView.UpdateUI(true);
                break;
            
            case ViewType.UIVIEW:
                m_UIEventHandler.UpdateView(true);
                break;
            
            case ViewType.ARCAMERAVIEW:
                break;

            case ViewType.STATVIEW:
                mEndGameView.UpdateUI(true);
                break;
        }
    }

    #region UNITY_MONOBEHAVIOUR_METHODS
    
    void OnApplicationPause(bool tf)
    {
        //On hitting the home button, the app tends to turn off the flash
        //So, setting the UI to reflect that
        m_UIEventHandler.SetToDefault(tf);
    }
    
    #endregion UNITY_MONOBEHAVIOUR_METHODS
    
    #region PRIVATE_METHODS


    private void OnEndGame()
    {
        mAboutView.UnLoadView();
        mActiveViewType = ViewType.STATVIEW;
        mEndGameView.LoadView();
    }
    private void OnSingleTapped()
    {
        if(mActiveViewType == ViewType.ARCAMERAVIEW )
        {
            // trigger focus once
            m_UIEventHandler.TriggerAutoFocus();
        }
    }
    
    private void OnDoubleTapped()
    {
        if(mActiveViewType == ViewType.ARCAMERAVIEW)
        {
            mActiveViewType = ViewType.UIVIEW;
        }
    }
    
    private void OnTappedOnGoToAboutPage()
    {
        mActiveViewType = ViewType.ABOUTVIEW;   
    }
    
    private void OnBackButtonTapped()
    {
        if(mActiveViewType == ViewType.ABOUTVIEW)
        {
            Application.Quit();
        }
        else if(mActiveViewType == ViewType.UIVIEW) //Hide UIMenu and Show ARCameraView
        {
            mActiveViewType = ViewType.ARCAMERAVIEW;
        }
        else if(mActiveViewType == ViewType.ARCAMERAVIEW) //if it's in ARCameraView
        {
            mActiveViewType = ViewType.ABOUTVIEW;
        }
        
    }
    
    private void OnTappedOnCloseButton()
    {
        mActiveViewType = ViewType.ARCAMERAVIEW;
    }
    
    private void OnAboutStartButtonTapped()
    {
        mActiveViewType = ViewType.ARCAMERAVIEW;
    }

    private void OnRestartButtonTapped()
    {
        mActiveViewType = ViewType.ARCAMERAVIEW;
        gM.resetGame();
    }
    private IEnumerator LoadAboutPageForFirstTime()
    {
        yield return new WaitForSeconds(mSecondsVisible);
        mSplashView.UnLoadView();
        mActiveViewType = ViewType.ABOUTVIEW;
        mAboutView.LoadView();
        m_UIEventHandler.Bind();
        yield return null;
    }
    #endregion PRIVATE_METHODS
    
}
