﻿using UnityEngine;
using System.Collections;
/*
 * Anthony Rosenbaum November 12-17 2014
 * vincismurf@gmail.com
 * 859 948 835
 * 
 * Derived from Base Bug:
 *  Has a shield that protected it, requires a double click to remove shield
 *  Same Strength as Base Bug, a bit faster thought
 */
public class AdvancedBug : BaseBug
{
    #region PUBLIC_MEMBER_VARIABLES
    public Texture withOutShield;
    public bool hasShield;
    #endregion PUBLIC_MEMBER_VARIABLES
    #region OVERRIDE_METHODS
    // Use this for initialization
    protected override void Start()
    {
        max = 0.8f;
        strength = 1;
        hasShield = true;
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }
    // Pass Along to Parent
    public override void clickBug()
    {
        if (hasShield == false)
        {
            handleClick();
        }
    }
    // Remove shield if bug has one, update texture
    public override void doubleClickBug()
    {
        if (hasShield)
        {
            hasShield = false;
            gameObject.renderer.material.mainTexture = withOutShield; 
        }
    }
    // Callback to update scoreboard
    protected override void kill()
    {
        gM.incAdvBugCount();
        if (squash)
        {
            squash.Play();
        }
    }
    #endregion OVERRIDE_METHODS
}
