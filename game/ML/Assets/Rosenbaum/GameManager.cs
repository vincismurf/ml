﻿using UnityEngine;
using System.Collections;
/* * 
 * Anthony Rosenbaum November 12-17 2014
 * vincismurf@gmail.com
 * 859 948 835
 * 
 * Manages Game Loop, calls spawner or AppManager, callback from Inputcontroller
 * Renders in game gui
 * Holds all stats
 */ 
public class GameManager : MonoBehaviour
{
    #region PUBLIC_MEMBER_VARIABLES
    public bool gameStarted = false;
    public bool targetAquired = false;
    public int baseBugsSquashed = 0;
    public int advancedBugsSquashed = 0;
    public int BossBugsSquashed = 0;
    public int currentLevel = 0;
 
    public System.Action OnGameOver;
    #endregion PUBLIC_MEMBER_VARIABLES
    #region PRIVATE_MEMBER_VARIABLES
    private Renderer terrain;
    private Camera cam;
    private BugSpawner spawner = null;
    private int maxLevels = 3;
    private float gameTime;
    private int countDownNumber = 0; // for counting till level starts
    private int countDownTimer = 0; // coundown of the levels
    private float countDownMax = 3.0f;
    private float gameTimerMax = 10.0f;
    private float deltaT = 0;
    #endregion PRIVATE_MEMBER_VARIABLES
    #region GAME_LOOP_METHODS
    // Use this for initialization
	void Start () {
        cam = GameObject.Find("ARCamera").GetComponent<Camera>();
        spawner  = GameObject.Find("BugSpawner").GetComponent<BugSpawner>();
	}
	
	// Update is called once per frame
	void Update () {
        gameLoop();
	}
    /*
     * If the game started and the target is a aquired spawn some bug and show the countdown.
     * First countdown to prep the player, then do the real clock with the bugs.
     * If no target, clear everything
     */ 
    private void gameLoop()
    {
        if (gameStarted)
        {
            if ( terrain != null)
            {
                float curTime = Time.fixedTime;
                float delta = curTime - gameTime;
                deltaT = delta;
                if (delta <= countDownMax)
                {
                    countDownNumber = (int)delta;
                }
                else
                    if (delta < (gameTimerMax * (currentLevel + 1) + countDownMax))
                    {
                        countDownTimer = (int)delta;
                        if (currentLevel < maxLevels)
                            spawner.checkSpawner(currentLevel);
                    }
                    else if (currentLevel < maxLevels)
                    {
                        levelOver();
                    }
            }
            else
            {
                spawner.clearAll();
            }
        }
    }
    //Game over clear every thing, end the game and call back to the AppManager
    public void gameOver()
    {
        Debug.Log("GameOver");
        spawner.clearAll();
        gameStarted = false;
        if (this.OnGameOver != null)
        {
            this.OnGameOver();
        }
    }
    // Reset the clock and up the level, go to game over if you reach max.
    private void levelOver()
    {
        currentLevel++;
        gameTime = Time.fixedTime;
        spawner.clearAll();

        if (currentLevel >= maxLevels)
       {            
            gameOver();
       }
    }
    //Started when Button pressed
    public void startGame()
    {
        gameStarted = true;
    }
    //Used in AppManager
    public bool hasStarted()
    {
        return gameStarted;
    }
    //Clear everything and reset the clock
    public void resetGame()
    {
        currentLevel = 0;
        baseBugsSquashed = 0;
        advancedBugsSquashed = 0;
        BossBugsSquashed = 0;
        gameTime = Time.fixedTime;
    }
    #endregion GAME_LOOP_METHODS
    #region TERRAIN_METHODS
    // Set the renderer from the target, start the clock
    public void setTerrain(Renderer r)
    {
        terrain = r;
        gameTime = Time.fixedTime;
    }
    // Accessor for Bugs
    public Renderer getTerrain()
    {
        return  terrain;
    }
    #endregion TERRAIN_METHODS
    #region BUG_INCREMENT_METHODS
    /*
     * Incrementors for Bugs
     */ 
    public void incBaseBugCount()
    {
        baseBugsSquashed++;
    }
    public void incAdvBugCount()
    {
        advancedBugsSquashed++;
    }
    public void incBossBugCount()
    {
        BossBugsSquashed++;
    }
    #endregion BUG_INCREMENT_METHODS
    #region BUG_GETTER_METHODS
    /*
     * Getters for Bugs
     */ 
    public int getBaseBugCount()
    {
        return baseBugsSquashed;
    }
    public int getAdvBugCount()
    {
        return advancedBugsSquashed;
    }
    public int getBossBugCount()
    {
        return BossBugsSquashed;
    }
    #endregion BUG_GETTER_METHODS
    #region INPUT_METHODS
    /*
     * Ray cast when tapped.
     */ 
    public void doubleTap()
    {
        if (cam)
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo))
            {
                Vector3 worldSpaceHitPoint = hitInfo.point;

                GameObject g = hitInfo.collider.gameObject;
                BaseBug b = g.GetComponent<BaseBug>();
                b.doubleClickBug();
            }
        }
    }
    /*
    * Ray cast when tapped.
    */ 
    public void singleTap()
    {
        if (cam)
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo))
            {
                Vector3 worldSpaceHitPoint = hitInfo.point;

                GameObject g = hitInfo.collider.gameObject;
                BaseBug b = g.GetComponent<BaseBug>();
                b.clickBug();

            }
        }
    }
    #endregion INPUT_METHODS
    #region GUI_METHODS
    // Render if the game is started, show countdown and level
    void OnGUI()
    {
        if (gameStarted)
        {
            // Make a background box
            GUI.Box(new Rect(10, 10, 150, 120), "");

            if (terrain != null)
            {
                int time = 0;
                string str = "";
                if (deltaT <= countDownMax)
                {
                    str = "Game Starts in : ";
                    time = (int)countDownMax - (int)deltaT;
                }
                else
                {
                    str = "Level Ends in : ";
                    float dif = ((gameTimerMax * (currentLevel + 1)) + countDownMax);
                    time = (int)dif - (int)deltaT;
                }

                GUI.Label(new Rect(15, 20, 150, 20), str + time.ToString());
                int level = currentLevel + 1;
                GUI.Label(new Rect(15, 40, 150, 20), "Level: " + level.ToString());
            }
            else
            {
                GUI.Label(new Rect(15, 20, 150, 20), "Need Target to Start!!");
            }
        }
    }
    #endregion GUI_METHODS
}
