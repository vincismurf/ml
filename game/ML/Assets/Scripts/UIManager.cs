﻿using UnityEngine;
using System.Collections;

public class UIManager : MonoBehaviour 
{
	#region Public Vars

	public UIPanel samplePanel;

	#endregion


	#region Private Vars

	#endregion



	#region Private Methods

	private void Start()
	{
		// Init
		ShowSamplePanel(true);
	}

	#endregion


	#region Public Methods

	public void ShowSamplePanel(bool show)
	{
		if (samplePanel)
			samplePanel.gameObject.SetActive(show);
		else
			Debug.LogWarning("No sample panel assigned in the inspector!");
	}

	public void BeginGame()
	{
		// Simple hide - could use a fade for a nice transition
		ShowSamplePanel(false);


		//TODO: Call game mechanics method
        Debug.Log("Let's start a Bug Squash games");


	}

	#endregion
}
