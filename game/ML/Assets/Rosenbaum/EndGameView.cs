﻿using UnityEngine;
using System.Collections;
/*
 * Anthony Rosenbaum November 12-17 2014
 * vincismurf@gmail.com
 * 859 948 835
 * 
 * Based on AboutScreenView, which has been modified as well
 */
public class EndGameView : MonoBehaviour
{
    #region PUBLIC_MEMBER_VARIABLES
    public System.Action OnRestartButtonTapped;
    public TextAsset m_EndGameText;
    public SampleAppUIBox mBox;
    #endregion PUBLIC_MEMBER_VARIABLES
    #region STYLE_VARIABLES
    GUIStyle mEndGameTitleBgStyle;
    GUIStyle mOKButtonBgStyle;
    #endregion STYLE_VARIABLES
    #region PRIVATE_MEMBER_VARIABLES
    private string mTitle;
    private const float ENDGAME_TEXT_MARGIN = 20.0f;
    private const float ENDGAME_BUTTON_VERTICAL_MARGIN = 10.0f;
	
    private GUISkin mUISkin;

    private Vector2 mScrollPosition;
    private float mStartButtonAreaHeight = 80.0f;
    private float mAboutTitleHeight = 80.0f;
    private GameManager gM = null;
    #endregion PRIVATE_MEMBER_VARIABLES
    #region UTIL_METHODS
    private static float DeviceDependentScale
    {
        get
        {
            if (Screen.width > Screen.height)
                return Screen.height / 480f;
            else
                return Screen.width / 480f;
        }
    }
    public void SetTitle(string title)
    {
        mTitle = title;
    }
    #endregion UTIL_METHODS
    #region ISampleAppUIView implementation
    public void LoadView()
    {
        m_EndGameText = Resources.Load("Rosenbaum_end") as TextAsset;
        gM = GameObject.Find("GameManager").GetComponent<GameManager>();
        mBox = new SampleAppUIBox();
        mEndGameTitleBgStyle = new GUIStyle();
        mOKButtonBgStyle = new GUIStyle();
        mEndGameTitleBgStyle.normal.background = Resources.Load("UserInterface/grayTexture") as Texture2D;
        mOKButtonBgStyle.normal.background = Resources.Load("UserInterface/capture_button_normal_XHigh") as Texture2D;

        mEndGameTitleBgStyle.font = Resources.Load("SourceSansPro-Regular_big_xhdpi") as Font;
        mOKButtonBgStyle.font = Resources.Load("SourceSansPro-Regular_big_xhdpi") as Font;
        if (Screen.dpi > 300)
        {
            // load and set gui style
            mUISkin = Resources.Load("UserInterface/ButtonSkinsXHDPI") as GUISkin;
            mUISkin.label.font = Resources.Load("SourceSansPro-Regular") as Font;
            mEndGameTitleBgStyle.font = Resources.Load("SourceSansPro-Regular_big_xhdpi") as Font;
            mOKButtonBgStyle.font = Resources.Load("SourceSansPro-Regular_big_xhdpi") as Font;

        }
        else if (Screen.dpi > 260)
        {
            // load and set gui style
            mUISkin = Resources.Load("UserInterface/ButtonSkins") as GUISkin;
            mUISkin.label.font = Resources.Load("SourceSansPro-Regular") as Font;
            mEndGameTitleBgStyle.font = Resources.Load("SourceSansPro-Regular_big_xhdpi") as Font;
            mOKButtonBgStyle.font = Resources.Load("SourceSansPro-Regular_big_xhdpi") as Font;

        }
        else if (Screen.height == 1848 && Screen.width == 1200)
        {
            mUISkin = Resources.Load("UserInterface/ButtonSkinsXHDPI") as GUISkin;
            mUISkin.label.font = Resources.Load("SourceSansPro-Regular") as Font;
            mEndGameTitleBgStyle.font = Resources.Load("SourceSansPro-Regular_big_xhdpi") as Font;
            mOKButtonBgStyle.font = Resources.Load("SourceSansPro-Regular_big_xhdpi") as Font;
        }
        else
        {
            // load and set gui style
            mUISkin = Resources.Load("UserInterface/ButtonSkinsSmall") as GUISkin;
            mUISkin.label.font = Resources.Load("SourceSansPro-Regular_Small") as Font;
            mEndGameTitleBgStyle.font = Resources.Load("SourceSansPro-Regular") as Font;
            mOKButtonBgStyle.font = Resources.Load("SourceSansPro-Regular") as Font;
        }

#if UNITY_IPHONE
        if(Screen.height > 1500 ){
            // Loads the XHDPI sources for the iPAd 3
            mUISkin = Resources.Load("UserInterface/ButtonSkinsiPad3") as GUISkin;
            mUISkin.label.font = Resources.Load("SourceSansPro-Regular_big_iPad3") as Font;
            mEndGameTitleBgStyle.font = Resources.Load("SourceSansPro-Regular_big_iPad3") as Font;
            mOKButtonBgStyle.font = Resources.Load("SourceSansPro-Regular_big_iPad3") as Font;
        }

#endif

        mOKButtonBgStyle.normal.textColor = Color.white;
        mEndGameTitleBgStyle.alignment = TextAnchor.MiddleLeft;
        mOKButtonBgStyle.alignment = TextAnchor.MiddleCenter;

    }
    public void UpdateUI(bool tf)
    {
        if (!tf)
            return;
        float scale = 1 * DeviceDependentScale;
        mAboutTitleHeight = 80.0f * scale;
        mBox.Draw();
        GUI.Box(new Rect(0, 0, Screen.width, mAboutTitleHeight), string.Empty, mEndGameTitleBgStyle);
        GUI.Box(new Rect(ENDGAME_TEXT_MARGIN * DeviceDependentScale, 0, Screen.width, mAboutTitleHeight), mTitle, mEndGameTitleBgStyle);
        float width = Screen.width / 1.5f;
        //float height = startButtonStyle.normal.background.height * scale;
        float height = mOKButtonBgStyle.normal.background.height * scale;

        mStartButtonAreaHeight = height + 2 * (ENDGAME_BUTTON_VERTICAL_MARGIN * scale);
        float left = Screen.width / 2 - width / 2;
        float top = Screen.height - mStartButtonAreaHeight + ENDGAME_BUTTON_VERTICAL_MARGIN * scale;

        GUI.skin = mUISkin;

        GUILayout.BeginArea(new Rect(ENDGAME_TEXT_MARGIN * DeviceDependentScale,
                                 mAboutTitleHeight + 5 * DeviceDependentScale,
                                 Screen.width - (ENDGAME_TEXT_MARGIN * DeviceDependentScale),
                                 Screen.height - (mStartButtonAreaHeight) - mAboutTitleHeight - 5 * DeviceDependentScale));

        mScrollPosition = GUILayout.BeginScrollView(mScrollPosition, false, false, GUILayout.Width(Screen.width - (ENDGAME_TEXT_MARGIN * DeviceDependentScale)),
            GUILayout.Height(Screen.height - mStartButtonAreaHeight - mAboutTitleHeight));

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        string txt = "Basic Bug Squashed " + gM.getBaseBugCount().ToString() + "\n " + "Advanced Bug Squashed " + gM.getAdvBugCount().ToString() + "\n " + "Boss Bug Squashed " + gM.getBossBugCount().ToString() + "\n ";

        GUILayout.Label(m_EndGameText.text +  txt);

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.EndScrollView();

        GUILayout.EndArea();

        // if button was pressed, remember to make sure this event is not interpreted as a touch event somewhere else
        if (GUI.Button(new Rect(left, top, width, height), "Restart", mOKButtonBgStyle))
        {
            if (this.OnRestartButtonTapped != null)
            {
                this.OnRestartButtonTapped();
            }
        }

    }
    public void UnLoadView()
    {
    }

    #endregion ISampleAppUIView Implementation
}
