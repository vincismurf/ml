﻿using UnityEngine;
using System.Collections.Generic;
/* 
 * Anthony Rosenbaum November 12-17 2014
 * vincismurf@gmail.com
 * 859 948 835
 * 
 * Responsible for adding and remove bugs. 
 * Manger will spawn, place and rotate bug.
 */
public class BugSpawner : MonoBehaviour
{

    #region PRIVATE_MEMBER_VARIABLES
    private BaseBug bb;
    private AdvancedBug ab;
    private BossBug Bb;
    private GameManager gM;
    private int currentLevel = 0;
    private Vector2 size;
    #endregion PRIVATE_MEMBER_VARIABLES
    #region PUBLIC_MEMBER_VARIABLES
    public int maxBugs = 1;
    public List<GameObject> bugs;
    #endregion PUBLIC_MEMBER_VARIABLES
    #region SPAWN_UTIL_METHODS
    // Use this for initialization
	void Start () {
        bugs = new List<GameObject>();
        bb = GameObject.Find("BaseBug").GetComponent<BaseBug>();
        ab = GameObject.Find("AdvancedBug").GetComponent<AdvancedBug>();
        Bb = GameObject.Find("BossBug").GetComponent<BossBug>();
        gM = GameObject.Find("GameManager").GetComponent<GameManager>();
	}
    //Remove all bugs and clear the list
    public void clearAll()
    {
        foreach (GameObject bug in bugs)
        {
            Destroy(bug, 0.1f);
        }
        bugs.Clear();
    }
    // If there is room for a new bug spawn one.
    public void checkSpawner(int level)
    {
        currentLevel = level;
        if (bugs.Count < maxBugs)
            spawnBug();
    }
    // Choose bug based on level
    private GameObject chooseBug()
    {
        GameObject gO = bb.gameObject;
        switch (currentLevel)
        {
            case 0: gO = bb.gameObject;
                break;
            case 1: gO = ab.gameObject;
                break;
            case 2: gO = Bb.gameObject;
                break;
            default : Debug.Log("BugSpawner:: Level " + currentLevel + " not supported" );
                break;
        }
        return gO;
    }
    
    // Choose a side, Get a postion, Get a Type, Spawn, Rotate, Set parent and scale!
    private void spawnBug()
    {        
        size = gM.getTerrain().gameObject.transform.parent.GetComponent<ImageTargetBehaviour>().GetSize();
        int side = Random.Range(0, 3);
        Vector3 bugPos = gM.getTerrain().gameObject.transform.position;
        Quaternion bugRot = gM.getTerrain().gameObject.transform.rotation;

        switch (side)
        {
            case 0: bugPos = spawnBehind();
                break;
            case 1: bugPos = spawnInFront();
                break;
            case 2: bugPos = spawnRight();
                break;
            case 3: bugPos = spawnLeft();
                break;
            default: Debug.Log("Side " + side + "not supported");
                break;
        }

        GameObject bugType = chooseBug();
        GameObject bug = (GameObject)Instantiate(bugType, bugPos, Quaternion.identity);
        Quaternion newRot = bug.transform.rotation;
        switch (side)
        {
            case 0: 
                break;
            case 1: newRot *= Quaternion.AngleAxis(180, bug.transform.up);
                break;
            case 2: newRot *= Quaternion.AngleAxis(270, bug.transform.up);
                break;
            case 3: newRot *= Quaternion.AngleAxis(90, bug.transform.up);
                break;
            default: Debug.Log("Side " + side + "not supported");
                break;
        }

        bug.transform.rotation = newRot;
        bug.transform.parent = gM.getTerrain().gameObject.transform.parent;
        bug.transform.localScale = new Vector3(0.02f, 0.02f, 0.02f);
        bugs.Add(bug);   
    }
    #endregion SPAWN_UTIL_METHODS
    #region SPAWN_POS_METHODS
    /*
     *  Four Methods to place the bug in front/behind/left/right of terrains center point
     *  using Bug's forward vector as referance
     */
    private Vector3 spawnInFront()
    {     
        Vector3 t = gM.getTerrain().gameObject.transform.position;
        t.z -= (size.y / 2 - 10);
        float min = t.x - (size.x/2 - 20);
        float max = t.x + (size.x/2 - 20);
        t.x = Random.Range(min, max);
        return t;
    }
    private Vector3 spawnBehind()
    { 
        Vector3 t = gM.getTerrain().gameObject.transform.position;
        t.z += (size.y / 2 - 10);
        float min = t.x - (size.x / 2 - 20);
        float max = t.x + (size.x / 2 - 20);
        t.x = Random.Range(min, max);
        return t;
    }
    private Vector3 spawnRight()
    {    
        Vector3 t = gM.getTerrain().gameObject.transform.position;
        t.x -= (size.x / 2 - 20);
        float min = t.z - (size.y / 2 - 10);
        float max = t.z + (size.y / 2 - 10);
        t.z = Random.Range(min, max);
        return t;
    }
    private Vector3 spawnLeft()
    { 
        Vector3 t = gM.getTerrain().gameObject.transform.position;
        t.x += (size.x / 2 - 20);
        float min = t.z - (size.y / 2 - 10);
        float max = t.z + (size.y / 2 - 10);
        t.z = Random.Range(min, max);
        return t;
    }
    #endregion SPAWN_POS_METHODS

}
